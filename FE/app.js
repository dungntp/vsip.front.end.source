console.log('Load app js!')

import $ from 'jquery'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'

// import Swiper from 'swiper'
// import 'swiper/dist/css/swiper.min.css'
// import 'swiper/dist/js/swiper.min.js'

import './scroll';
import fullpage from "./fullpage.js"

import 'jquery-parallax.js'
import 'jquery-parallax.js/parallax.js'

import 'slick-carousel'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

import 'tooltipster/dist/css/tooltipster.bundle.min.css'
import 'tooltipster/dist/js/tooltipster.bundle.min.js'

import 'malihu-custom-scrollbar-plugin'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js'

import 'jquery-countdown'

import 'select2/dist/css/select2.css'
import 'select2/dist/js/select2.full.js'
import 'select2-bootstrap-theme/dist/select2-bootstrap.css'

import 'lightbox2'
import 'lightbox2/dist/css/lightbox.css'
import 'lightbox2/dist/js/lightbox.js'

import 'animate.css/animate.min.css'

import WOW from 'wow.js'


import './scss/style.scss'


$(".content-scrollbar").mCustomScrollbar();

$('#clock').countdown('2018/10/10').on('update.countdown', function(event) {
    var $this = $(this).html(event.strftime(''
      + '<span>%-d</span> day%!d '
      + '<span>%H</span> hr '
      + '<span>%M</span> min '
      + '<span>%S</span> sec'));
  });

  $('#clock-2').countdown('2018/10/10').on('update.countdown', function(event) {
    var $this = $(this).html(event.strftime(''
      + '<span>%-d</span> day%!d '
      + '<span>%H</span> hr '
      + '<span>%M</span> min '
      + '<span>%S</span> sec'));
  });

$('.banner-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 2000,
});
$('.about-slider-img').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 1000,
});

$('.about-milestones__slider').slick({
    slidesToShow: 5,
    slidesToScroll: 2,
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: true,
    dots: false,
});

$('.upcoming-events__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 2000,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
});

$('.news-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
});

$('.recent-event__slider').slick({
    slidesToShow: 3,
    slidesToScroll: 2,
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: true,
    dots: true,
});
$('.all-event__slider').slick({
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
});
$('.charity__slider').slick({
    slidesToShow: 3,
    slidesToScroll: 2,
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: true,
    dots: true,
});
$('.csr__sider').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
});
$('.news-page__slider').slick({
    slidesToShow: 3,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    dots: true,
});

$('.map-content__inner').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: true,
});


var banner1=require('./images/b-banner-1.jpg');
var banner2=require('./images/b-banner-2.jpg');
var banner3=require('./images/b-banner-3.jpg');
var banner4=require('./images/b-banner-4.jpg');
var banner5=require('./images/b-banner-5.jpg');
var banner6=require('./images/b-banner-6.jpg');
var banner7=require('./images/b-banner-7.jpg');


$('.binh_duong1').on('click', function(){
    $('.business-inner').css("background", "url("+banner3+") 100% 100%");
});
$('.binh_duong2').on('click', function(){
    $('.business-inner').css("background", "url("+banner3+") 100% 100%");
});
$('.bacninh').on('click', function(){
    $('.business-inner').css("background", "url("+banner1+") 100% 100%");
});
$('.haiphong').on('click', function(){
  $('.business-inner').css("background", "url("+banner2+") 100% 100%");
});
$('.quangngai').on('click', function(){
  $('.business-inner').css("background", "url("+banner4+") 100% 100%");
});
$('.haiduong').on('click', function(){
  $('.business-inner').css("background", "url("+banner6+") 100% 100%");
});
$('.nghean').on('click', function(){
  $('.business-inner').css("background", "url("+banner5+") 100% 100%");
});

$('.binh_duong1').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner3+") 100% 100%");
    $('#binh_duong1').addClass('active in');
});
$('.binh_duong2').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner3+") 100% 100%");
    $('#binh_duong2').addClass('active in');
});
$('.bacninh').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner1+") 100% 100%");
    $('#bacninh').addClass('active in');
});
$('.haiphong').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner2+") 100% 100%");
    $('#haiphong').addClass('active in');
});
$('.quangngai').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner4+") 100% 100%");
    $('#quangngai').addClass('active in');
});
$('.haiduong').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner6+") 100% 100%");
    $('#haiduong').addClass('active in');
});
$('.nghean').mouseover(function(){
    $('.tab-pane').removeClass('active in');
    $('.business-inner').css("background", "url("+banner7+") 100% 100%");
    $('#nghean').addClass('active in');
});

$('.map-detail').mouseover(function(){
    $('.map-content').removeClass('active');
    $(this).next().addClass("active"); 
});

$('.map-detail').mouseover(function(){
    $('.map-detail').removeClass('active');
    $(this).addClass("active"); 
});


new WOW().init();

new fullpage('#fullpage', {
    //options here
    licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
    autoScrolling:true,
    scrollHorizontally: true,
    scrollOverflow: true,
});

$('.select2').select2({
    theme: 'bootstrap'
});

$(document).ready(function(){
    lightbox.option({
        'resizeDuration': 500,
        'wrapAround': true
    })
});

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});

