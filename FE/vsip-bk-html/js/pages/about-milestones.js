$('.map-content__inner').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    // autoplay: true,
    // autoplaySpeed: 2000,
    arrows: true,
});