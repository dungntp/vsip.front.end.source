$('.news-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
});

var revapi;

revapi = jQuery('.tp-banner').revolution(
    {
        delay: 6000,
        startwidth: 1170,
        startheight: 700,
        hideThumbs: 10,
        fullWidth: "on",
        forceFullWidth: "on",
    });