$('.about-slider-img').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 1000,
});

var revapi;

revapi = jQuery('.tp-banner').revolution(
    {
        delay: 6000,
        startwidth: 1170,
        startheight: 700,
        hideThumbs: 10,
        fullWidth: "on",
        forceFullWidth: "on",
    });

revapi = jQuery('.tp-banner2').revolution(
    {
        delay: 8000,
        startwidth: 1170,
        startheight: 700,
        hideThumbs: 10,
        fullWidth: "on",
        forceFullWidth: "on",
    });