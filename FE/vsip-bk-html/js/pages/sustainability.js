$('.charity__slider').slick({
    slidesToShow: 3,
    slidesToScroll: 2,
    // autoplay: true,
    // autoplaySpeed: 2000,
    dotsClass: 'slick-dots',
    arrows: true,
    dots: true,
});

$('.csr__sider').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
});