console.log('Load app js!')

// $(".content-scrollbar").mCustomScrollbar();

// for event and upcoming event page.php
// $('.recent-event__slider').slick({
//     slidesToShow: 3,
//     slidesToScroll: 2,
//     // autoplay: true,
//     // autoplaySpeed: 2000,
//     arrows: true,
//     dots: true,
// });

// $('.all-event__slider').slick({
//     slidesToShow: 5,
//     slidesToScroll: 2,
//     autoplay: true,
//     autoplaySpeed: 2000,
//     arrows: false,
//     dots: true,
// });

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
    });
});


;(function($, window, Unibail, undefined) {
    'use strict';
  
    var pluginName = 'toggle-element';
  
    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
  
    Plugin.prototype = {
      init: function() {
        var that = this;
            that.inItToggle();

            this.vars = {
                elFocusPrev: null
            };
      },
      inItToggle: function() {
        var that = this,
            el = that.element,
            options = that.options,
            controlEl = el.find('a'),
            contentEl = controlEl.siblings('ul');
  
        controlEl.on('click.' + pluginName , function(e) {
            e.preventDefault();

            var self = $(this),
                hasSibling = self.next().is('ul'),
                parents = self.parents('li'),
                parent = self.parent('li'),
                parentSibling = parent.siblings();

            if (!parent.hasClass('active') && !hasSibling) {
                
                if (parents.hasClass('active')) {
                    options.focus ? parents.addClass('focus') :undefined;
                    parents.removeClass('active');
                }
                parent.hasClass('active') ? parent.removeClass('active') : undefined;
                window.location.href = self.attr('href');
            }

            if (parent.hasClass('active') && !hasSibling) {
                if (parents.hasClass('active') && !hasSibling) {
                    window.location.href = self.attr('href');
                }
                return;
            }

            if (parent.hasClass('active') && hasSibling) {
                parent.removeClass('active');
                if (options.focus) {
                    that.vars.elFocusPrev.addClass('focus');
                    that.vars.elFocusPrev = null;
                }
                return;
            } 

            parentSibling.each(function() {
                var currentParent = $(this);
                if (currentParent.hasClass('focus') && options.focus) {
                    currentParent.removeClass('focus');
                    currentParent.unbind('hover');
                    that.vars.elFocusPrev = currentParent;
                }
                currentParent.hasClass('active') ? currentParent.removeClass('active') : undefined;
                
            });

            if (!parent.hasClass('active') && hasSibling) {
                parent.addClass('active');
            } 
        });
      },
  
      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };
  
    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };
  
    $.fn[pluginName].defaults = {
        focus: false
    };
  
    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  
  }(window.jQuery, window));