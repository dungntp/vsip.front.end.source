<!DOCTYPE html>
<html lang="en">
<head>
    <title>Trang chủ</title><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=vietnamese" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/navstylechange.css" media="screen" />
	<link href="css/app.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet">
</head>
<body>
    <?php 
        $str = $_SERVER['REQUEST_URI'];
        $url = explode('/', $str);
    ?>
    <header>
        <div class="top-header">
            <div class="container"><a class="logo pull-left" href="index.php" title=""><img src="images/logo.png" alt=""></a>
                <form class="pull-right form-inline"><i class="fa fa-search" aria-hidden="true"></i>
                    <div class="form-group pull-right">
                        <label for="searchForm">Search</label>
                        <input class="form-control" id="searchForm" type="text" placeholder="">
                    </div>
                    <div class="dropdown pull-right">
                        <button class="btn btn-default dropdown-toggle" id="dropdownMenu1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">VN<span class="caret"></span></button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">VN</a></li>
                            <li><a href="#">EN</a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
        
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav menu">
                        <?php if($url[3] == '' || $url[3] == 'index.php'){ ?>
                        <li class="active"><a href="index.php" title="">Home</a></li>
                        <li><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.php" title="">Contact us</a></li>
                        <?php }
                        else if($url[3] == 'about-glance.php' || $url[3] == 'about-holding.php' || $url[3] == 'about-milestones.php' || $url[3] == 'about-culture.php' || $url[3] == 'about-award.php'){ ?>
                        <li><a href="index.php" title="">Home</a></li>
                        <li class="active"><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.php" title="">Contact us</a></li>                
                        <?php 
                        }
                        else if($url[3] == 'our-business-ip.php' || $url[3] == 'our-business-cr.php' || $url[3] == 'our-investment.php' || $url[3] == 'our-service.php' || $url[3] == 'our-pre-licensing.php' || $url[3] == 'our-post-licensing.php' || $url[3] == 'our-business-ip-1.php' || $url[3] == 'our-business-ip-2.php' || $url[3] == 'our-business-ip-3.php'){ ?>
                        <li><a href="index.php" title="">Home</a></li>
                        <li><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li class="active"><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.php" title="">Contact us</a></li>    
                        <?php 
                        }
                        else if($url[3] == 'new.php' || $url[3] == 'new.php#tenants' || $url[3] == 'new.php#upcoming'){ ?>
                        <li><a href="index.php" title="">Home</a></li>
                        <li><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.php" title="">Contact us</a></li>
                        <?php 
                        }
                        else if($url[3] == 'sustainability.php' || $url[3] == 'sustainability.php#charity' || $url[3] == 'sustainability.php#csr' || $url[3] == 'sustainability.php#qhse'){ ?>
                        <li><a href="index.php" title="">Home</a></li>
                        <li><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.php" title="">Contact us</a></li>
                        <?php 
                        }
                        else if($url[3] == 'contact-us.php'){ ?>
                        <li><a href="index.php" title="">Home</a></li>
                        <li><a href="" title="">About Us</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="about-glance.php" title="">At A Glance</a></li>
                                <li><a href="about-holding.php" title="">Share Holding</a></li>
                                <li> <a href="about-milestones.php" title="">Milestones</a></li>
                                <li><a href="about-culture.php" title="">Vision & Culture</a></li>
                                <li><a href="about-award.php" title="">Awards</a></li>
                            </ul>
                        </li>
                        <li><a href="" title="">Our Business</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="our-business-ip.php" title="">Industrial Park</a></li>
                                <li><a href="our-business-cr.php" title="">Commercial & Residential</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" title="">News & Events</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="new.php" title="">VSIP News</a></li>
                                <li><a href="new.php#tenants" title="">Tenants News</a></li>
                                <li><a href="new.php#upcoming" title="">Upcoming Events</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="sustainability.php" title="">Sustainability</a><i class="fa fa-angle-down" aria-hidden="true"></i>
                            <ul class="sub-menu">
                                <li><a href="sustainability.php#charity" title="">Charity Events</a></li>
                                <li><a href="sustainability.php#csr" title="">CSR Programme</a></li>
                                <li><a href="sustainability.php#qhse" title="">QHSE Policy</a></li>
                            </ul>
                        </li>
                        <li class="active"><a href="contact-us.php" title="">Contact us</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>