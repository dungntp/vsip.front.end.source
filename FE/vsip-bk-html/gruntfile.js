module.exports = function(grunt) {

    grunt.initConfig({
        clean: {
            build: ['dist']
        },
        concat: {
            css: {
                files: [
                    {
                        src: [
                            'css/libs/animate.min.css'
                        ],
                        dest: 'dist/css/global/global.css'
                    }
                ]
            },
            js: {
                files: [
                    {
                        src: [
                            'js/libs/jquery-3.3.1.js',
                            'js/libs/modernizr.2.8.3.js',
                            'js/libs/bootstrap.min.js',
                            'js/libs/iscroll.js.js',
                            'js/libs/fullpage.min.js',
                            'js/libs/parallax.min.js',
                            'js/libs/slick.min.js',
                            'js/libs/tooltipster.bundle.min.js',
                            'js/libs/jquery.countdown.min.js',
                            'js/libs/jquery.mCustomScrollbar.js',
                            'js/libs/select2.min.js',
                            'js/libs/select2.full.min.js',
                            'js/libs/lightbox.min.js',
                            'js/libs/wow.min.js',
                            'js/libs/jquery.themepunch.plugins.min.js',
                            'js/libs/jquery.themepunch.revolution.min.js'
                        ],
                        dest: 'dist/script/global/global.js'
                    },
                    {
                        src: [
                            'js/components/banner-slider.js',
                            'js/pages/about-award.js',
                        ],
                        dest: 'dist/script/pages/about-award.js'
                    },
                    {
                        src: [
                            'js/pages/about-culture.js',
                        ],
                        dest: 'dist/script/pages/about-culture.js'
                    },
                    {
                        src: [
                            'js/pages/about-glance.js',
                        ],
                        dest: 'dist/script/pages/about-glance.js'
                    },
                    {
                        src: [
                            'js/pages/about-holding.js',
                        ],
                        dest: 'dist/script/pages/about-holding.js'
                    },
                    {
                        src: [
                            'js/components/map.js',
                            'js/pages/about-milestones.js',
                            'js/components/milestone-slider.js',
                        ],
                        dest: 'dist/script/pages/about-milestones.js'
                    },
                    {
                        src: [
                            'js/pages/contact-us.js',
                        ],
                        dest: 'dist/script/pages/contact-us.js'
                    },
                    {
                        src: [
                            'js/components/project-and-location-list.js',
                            'js/components/banner-slider.js',
                            'js/pages/homepage.js',
                        ],
                        dest: 'dist/script/pages/homepage.js'
                    },
                    {
                        src: [
                            'js/pages/new-detail-2.js',
                        ],
                        dest: 'dist/script/pages/new-detail-2.js'
                    },
                    {
                        src: [
                            'js/pages/new-detail.js',
                        ],
                        dest: 'dist/script/pages/new-detail.js'
                    },
                    {
                        src: [
                            'js/components/count-down.js',
                            'js/components/banner-slider.js',
                            'js/pages/new.js',
                        ],
                        dest: 'dist/script/pages/new.js'
                    },
                    {
                        src: [
                            'js/pages/our-business-cr.js',
                        ],
                        dest: 'dist/script/pages/our-business-cr.js'
                    },
                    {
                        src: [
                            'js/components/milestone-slider.js',
                            'js/pages/our-business-ip-1.js',
                        ],
                        dest: 'dist/script/pages/our-business-ip-1.js'
                    },
                    {
                        src: [
                            'js/pages/our-business-ip-2.js',
                        ],
                        dest: 'dist/script/pages/our-business-ip-2.js'
                    },
                    {
                        src: [
                            'js/pages/our-business-ip-3.js',
                        ],
                        dest: 'dist/script/pages/our-business-ip-3.js'
                    },
                    {
                        src: [
                            'js/components/project-and-location-list.js',
                            'js/pages/our-business-ip.js',
                        ],
                        dest: 'dist/script/pages/our-business-ip.js'
                    },
                    {
                        src: [
                            'js/pages/our-investment.js',
                        ],
                        dest: 'dist/script/pages/our-investment.js'
                    },
                    {
                        src: [
                            'js/pages/our-post-licensing.js',
                        ],
                        dest: 'dist/script/pages/our-post-licensing.js'
                    },
                    {
                        src: [
                            'js/pages/our-pre-licensing.js',
                        ],
                        dest: 'dist/script/pages/our-pre-licensing.js'
                    },
                    {
                        src: [
                            'js/pages/our-service.js',
                        ],
                        dest: 'dist/script/pages/our-service.js'
                    },
                    {
                        src: [
                            'js/components/banner-slider.js',
                            'js/pages/sustainability.js',
                        ],
                        dest: 'dist/script/pages/sustainability.js'
                    },
                ]
            }
        },
        copy: {
            main: {
                files: [
                    { expand: true, cwd: 'Content/images/', src: ['**'], dest: 'dist/content/images/' },
                    { expand: true, cwd: 'Content/css/images/', src: ['**'], dest: 'dist/content/css/images/' },
                    { expand: true, cwd: 'Content/img/', src: ['**'], dest: 'dist/content/img/' },
                    { expand: true, cwd: 'Content/fonts/', src: ['**'], dest: 'dist/content/fonts/' }
                ]
            }
        },
        watch: {
            css: {
                files: ['css/**/*.css'],
                tasks: ['newer:concat:css']
            },
            js: {
                files: ['js/**/*.js'],
                tasks: ['newer:concat:js']
            }
        }
    });
  
    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-newer');
  
    // Default task(s).
    grunt.registerTask('default', ['build:dev']);
    grunt.registerTask('clean:all', ['clean']);
    grunt.registerTask('build:dev', ['clean:all', 'newer:concat:css', 'newer:concat:js', 'watch']);
  
  };