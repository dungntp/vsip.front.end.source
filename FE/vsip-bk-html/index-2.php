<?php include('header.php'); ?>
    <div class="banner-slider">
        <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> Sun Casa building on two counts of VSIP success</h1>
                    <p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. adipisicing elit. Expedita inventore </p>
                </div>
            </div>
            <img src="images/slider-update1a.jpg" alt="">
        </div>
        <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> JVSIP Nghe An celebrates new school year’s opening ceremony at Hung Tay primary school.</h1>
                    <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita inventore </p>
                </div>
            </div><img src="images/slider-update2a.jpg" alt="">
        </div>
        <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> Lorem, ipsum dolor sit amet consectetur adipisicing elit. </h1>
                    <p>  Lorem ipsum dolor sit amet consectetur adipisicing elit jointly develop the 1st.</p>
                </div>
            </div><img src="images/slider-update3.jpg" alt="">
        </div>
        <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> Japanese developer NISHI-NIPPON railroad signs agreement to jointly develop the 1st.</h1>
                    <p>  Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab! adipisicing elit. Expedita inventore </p>
                </div>
            </div><img src="images/slider-update4.jpg" alt="">
        </div>
        <!-- <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magni, debitis. adipisicing elit. Expedita inventore </h1>
                    <p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. adipisicing elit. Expedita inventore </p>
                </div>
            </div><img src="images/slider-update6.png" alt="">
        </div>
        <div class="banner-slider__item">
            <div class="banner-slider__inner"> 
                <div class="banner-slider__title">
                    <h1> Lorem ipsum dolor sit amet consectetur adipisicing elit. </h1>
                    <p>  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe molestias optio sequi itaque enim. Illo, quidem?</p>
                </div>
            </div><img src="images/slider-update7.png" alt="">
        </div> -->
    </div>
    <div class="mouse-icon">
        <div class="mouse-icon__inner"><img class="wow flash" src="images/mouse-icon2.png" alt=""><i class="wow flash fa fa-angle-double-down" aria-hidden="true"></i></div>
    </div>
		<div class="about-us">
			<div class="container">
				<div class="title-content">
					<h3><span>About us</span></h3>
				</div>
				<div class="about-us__inner clearfix">
					<div class="about-us__item"><a href="about-glance.php" title=""><img src="images/icon-1.png" alt=""><span>   At A Glance</span></a></div>
					<div class="about-us__item"><a href="about-holding.php" title=""><img src="images/icon-2.png" alt=""><span>   Share Holding</span></a></div>
					<div class="about-us__item"><a href="about-milestones.php" title=""><img src="images/icon-3.png" alt=""><span>   Milestones</span></a></div>
					<div class="about-us__item"><a href="about-culture.php" title=""><img src="images/icon-4.png" alt=""><span>   Vision & Culture</span></a></div>
					<div class="about-us__item"><a href="about-award.php" title=""><img src="images/icon-5.png" alt=""><span>   Awards     </span></a></div>
				</div>
			</div>
		</div>
		<div class="business content-item">
			<div class="bg-content-style"> 
				<div class="container">
					<div class="title-content">
						<h3 class="title-our-business">our business</h3><span></span>
					</div>
				</div>
			</div>
			<div class="width-100">
				<div class="business-inner">
					<div class="container">
						<div class="row">
							<div class="col-md-5">
								<ul class="business-list">
									<li class="li-title"><a href="">Industrial Park </a></li>
									<li class="active" role="presentation"><a href="">Our Project <img src="images/right-icon-2.png" alt=""></a>
										<ul>
											<li><a href="#">VSIP Binh Duong <img src="images/right-icon-2.png" alt=""></a>
												<ul>
													<li><a class="binh_duong1" title="" href="our-business-ip-1.php">VSIP I Binh Duong</a></li>
													<li><a class="binh_duong2" title="" href="our-business-ip-2.php">VSIP II Binh Duong</a></li>
												</ul>
											</li>
											<li><a class="bacninh" title="" href="our-business-ip-2.php">VSIP Bac Ninh <img src="images/right-icon-2.png" alt=""></a></li>
											<li><a class="haiphong" data-toggle="tab" href="#haiphong">VSIP Hai Phong <img src="images/right-icon-2.png" alt=""></a></li>
											<li><a class="quangngai" data-toggle="tab" href="#quangngai">VSIP Quang Ngai <img src="images/right-icon-2.png" alt=""></a></li>
											<li><a class="haiduong" data-toggle="tab" href="#haiduong">VSIP Hai Duong <img src="images/right-icon-2.png" alt=""></a></li>
											<li><a class="nghean" data-toggle="tab" href="#nghean">VSIP Nghe An <img src="images/right-icon-2.png" alt=""></a></li>
										</ul>
									</li>
									<li role="presentation"><a href="">Invesment Support <img src="images/right-icon-2.png" alt=""></a>
										<ul>
											<li><a href="our-investment.php">Invesment Incentives </a></li>
											<li><a href="our-service.php">One - Stop Service </a></li>
											<li><a href="our-pre-licensing.php">Pre - Licensing </a></li>
											<li><a href="post-pre-licensing.php">Post - Licensing </a></li>
										</ul>
									</li>
									<li class="li-title"><a href="">Commercial &amp; Residential </a></li>
									<li role="presentation"><a href="">Master Development <img src="images/right-icon-2.png" alt=""></a>
										<ul>
											<li><a href="our-business-ip-2.php">VSIP Binh Duong</a></li>
											<li><a href="">VSIP Bac Ninh</a>
												<ul>
													<li><a href="" title="">Bel Home Bac Ninh</a></li>
												</ul>
											</li>
											<li><a href="">VSIP Hai Phong</a></li>
											<li><a href="">VSIP Quang Ngai</a>
												<ul>
													<li><a href="" title="">Thiện Mỹ Lộc</a></li>
												</ul>
											</li>
											<li><a href="">VSIP Nghe An</a></li>
										</ul>
									</li>
									<li role="presentation"><a href="">Develop By VSIP <img src="images/right-icon-2.png" alt=""></a>
										<ul>
											<li><a href="our-business-ip-3.php">The Habitat</a></li>
											<li><a href="">Sun Casa</a></li>
											<li><a href="">Thien My Loc </a></li>
											<li><a href="">Casa Flora</a></li>
											<li><a href="">Belhomes</a></li>
											<li><a href="">...</a></li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="col-md-7">
								<div class="tab-content business-list-content">
									<div class="tab-pane fade active in" id="binh_duong1"><img src="images/our-business-01.jpg" alt=""></div>
									<div class="tab-pane fade" id="binh_duong2"><img src="images/our-business-06.jpg" alt=""></div>
									<div class="tab-pane fade" id="bacninh"><img src="images/our-business-02.jpg" alt=""></div>
									<div class="tab-pane fade" id="haiphong"><img src="images/our-business-03.jpg" alt=""></div>
									<div class="tab-pane fade" id="quangngai"><img src="images/our-business-04.jpg" alt=""></div>
									<div class="tab-pane fade" id="haiduong"><img src="images/our-business-05.jpg" alt=""></div>
									<div class="tab-pane fade" id="nghean"><img src="images/our-business-07.jpg" alt=""></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="news content-item">      
			<div class="container">
				<div class="title-content" id="news">
					<h3 class="our-news">News</h3><span></span>
				</div>
				<div class="news-slider">
					<div class="news-slider__item clearfix">
						<div class="news-slider__item--img pull-left"><img src="images/new-slider.jpg" alt="">
							<div class="news-slider__item--link-title"><a href="" title="">Sun Casa building on two counts of VSIP success adipisicing elit.</a></div>
						</div>
						<div class="news-slider__item--inner">
							<div class="clearfix"><a class="news-slider__item--title" href="" title="">Sun Casa building on two counts of VSIP success</a>
								<p class="news-slider__item--text">Riding on the success of its first two residential projects, The Habitat Binh Duong and Thien My Loc Quang Ngai, VSIP has unveiled its third resiential project known as Sun Casa. With modern design and ultimate convenience, Sun Casa will be the highlight of the mid-end townhouse segment in the Binh Duong real estate market in the coming time.</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="news-slider__item clearfix">
						<div class="news-slider__item--img pull-left"><img src="images/new-slider2.jpg" alt="">
							<div class="news-slider__item--link-title"><a href="" title="">The 14th VSIP Charity Day: A spring carnival for workers adipisicing elit.</a></div>
						</div>
						<div class="news-slider__item--inner">
							<div class="clearfix"><a class="news-slider__item--title" href="" title="">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci, nemo? Dicta, magnam.</a>
								<p class="news-slider__item--text">Riding on the success of its first two residential projects, The Habitat Binh Duong and Thien My Loc Quang Ngai, VSIP has unveiled its third resiential project known as Sun Casa. With modern design and ultimate convenience, Sun Casa will be the highlight of the mid-end townhouse segment in the Binh Duong real estate market in the coming time. </p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="news-slider__item clearfix">
						<div class="news-slider__item--img pull-left"><img src="images/new-slider3.jpg" alt="">
							<div class="news-slider__item--link-title"><a href="" title="">Sun Casa building on two counts of VSIP success adipisicing elit.</a></div>
						</div>
						<div class="news-slider__item--inner">
							<div class="clearfix"><a class="news-slider__item--title" href="" title="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, placeat.</a>
								<p class="news-slider__item--text">Riding on the success of its first two residential projects, The Habitat Binh Duong and Thien My Loc Quang Ngai, VSIP has unveiled its third resiential project known as Sun Casa. With modern design and ultimate convenience, Sun Casa will be the highlight of the mid-end townhouse segment in the Binh Duong real estate market in the coming time.</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="news-slider__item clearfix">
						<div class="news-slider__item--img pull-left"><img src="images/new-slider4.jpg" alt="">
							<div class="news-slider__item--link-title"><a href="" title="">The 14th VSIP Charity Day: A spring carnival for workers adipisicing elit.</a></div>
						</div>
						<div class="news-slider__item--inner">
							<div class="clearfix"><a class="news-slider__item--title" href="" title="">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci, nemo? Dicta, magnam.</a>
								<p class="news-slider__item--text">Riding on the success of its first two residential projects, The Habitat Binh Duong and Thien My Loc Quang Ngai, VSIP has unveiled its third resiential project known as Sun Casa. With modern design and ultimate convenience, Sun Casa will be the highlight of the mid-end townhouse segment in the Binh Duong real estate market in the coming time.</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="news-slider__item clearfix">
						<div class="news-slider__item--img pull-left"><img src="images/new-slider5.jpg" alt="">
							<div class="news-slider__item--link-title"><a href="" title="">Sun Casa building on two counts of VSIP success adipisicing elit.</a></div>
						</div>
						<div class="news-slider__item--inner">
							<div class="clearfix"><a class="news-slider__item--title" href="" title="">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt, accusamus nobis.</a>
								<p class="news-slider__item--text">Riding on the success of its first two residential projects, The Habitat Binh Duong and Thien My Loc Quang Ngai, VSIP has unveiled its third resiential project known as Sun Casa. With modern design and ultimate convenience, Sun Casa will be the highlight of the mid-end townhouse segment in the Binh Duong real estate market in the coming time.</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="lastest-new__item clearfix"><a class="astest-new__item--img post-thumb pull-left" href="new-detail-2.php" title=""><img src="images/new-ud-img3.jpg" alt=""></a>
							<div class="lastest-new__item--inner"><a class="lastest-new__item--title" href="new-detail-2.php" title="">VSIP Hai Duong Opens New Office &amp; Attains ISO 9001:2015 &amp; OHSAS 18001:2007 Certificates Awarding</a>
								<p class="lastest-new__item--text">Hai Duong, 26th April 2017 &ndash; Today VSIP Hai Duong Ltd., Co.  held the grand opening of its office and ISO 9001:2015 and OHSAS...</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="new-detail-2.php" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="lastest-new__item clearfix"><a class="astest-new__item--img post-thumb pull-left" href="new-detail-2.php" title=""><img src="images/new-ud-img4.jpg" alt=""></a>
							<div class="lastest-new__item--inner"><a class="lastest-new__item--title" href="new-detail-2.php" title="">JVSIP Nghe An celebrates new school year&rsquo;s opening ceremony at Hung Tay primary school.</a>
								<p class="lastest-new__item--text">Nghe An, 05 September 2017 - At the School Year Opening Ceremony of Hung Tay Primary School...</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="new-detail-2.php" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="lastest-new__item clearfix"><a class="astest-new__item--img post-thumb pull-left" href="new-detail-2.php" title=""><img src="images/new-ud-img1.jpg" alt=""></a>
							<div class="lastest-new__item--inner"><a class="lastest-new__item--title" href="new-detail-2.php" title="">VSIP Hai Duong Opens New Office &amp; Attains ISO 9001:2015 &amp; OHSAS 18001:2007 Certificates Awarding</a>
								<p class="lastest-new__item--text">Hai Duong, 26th April 2017 &ndash; Today VSIP Hai Duong Ltd., Co.  held the grand opening of its office and ISO 9001:2015 and OHSAS...</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="new-detail-2.php" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="lastest-new__item clearfix"><a class="astest-new__item--img post-thumb pull-left" href="new-detail-2.php" title=""><img src="images/new-ud-img2.jpg" alt=""></a>
							<div class="lastest-new__item--inner"><a class="lastest-new__item--title" href="new-detail-2.php" title="">JVSIP Nghe An celebrates new school year&rsquo;s opening ceremony at Hung Tay primary school.</a>
								<p class="lastest-new__item--text">Nghe An, 05 September 2017 - At the School Year Opening Ceremony of Hung Tay Primary School...</p>
								<div class="lastest-new__item--readmore"><a class="news__item--link-icon" href="new-detail-2.php" title=""><img src="images/icon-detail.png" alt=""></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="sustainability content-item bg-content-style">
			<div class="container">           
				<div class="title-content" id="sustainability">
					<h3 class="our-sustainability">Sustainability</h3><span></span>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="news__item">
							<div class="news__item--img post-thumb"><a href="" title=""><img class="responsive" src="images/new-4.jpg" alt=""></a></div><a class="news__item--title" href="" title="">The 14th VSIP Charity Day: A spring carnival for workers</a>
							<p class="news__item--text">Themed &ldquo;Spring Carnival 2017&rdquo;, the 14th VSIP Charity Day raised US$50,000 for VSIP Charity Fund; - VSIP Music Competition&rsquo;s final round with 34 performances;</p>
							<div style="text-align: right;"><a class="news__item--link-icon" href="" title=""><img src="images/icon-detail.png" alt=""></a></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="news__item">
							<div class="news__item--img post-thumb"><a href="" title=""><img class="responsive" src="images/new-5.jpg" alt=""></a></div><a class="news__item--title" href="" title="">The 14th VSIP Charity Day: A spring carnival for workers</a>
							<p class="news__item--text">The Vietnam Singapore Industrial Park (VSIP) Management Board, the VSIP Trade Union and VSIP JV Company successfully raised US$40,000 at the 12th VSIP Charity Day on May 10 and 11, 2014.</p>
							<div style="text-align: right;"><a class="news__item--link-icon" href=""><img src="images/icon-detail.png" alt=""></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="events content-item">
			<div class="width-100">
				<div class="container">
					<div class="title-content" id="events">
						<h3 class="our-events">Events</h3><span>   </span>
					</div>
				</div>
				<div class="events-img"><img src="images/img-update3.png" alt=""></div>
				<div class="upcoming-events">
					<div class="container">
						<div class="row">
							<div class="col-md-3"><img class="pull-left" src="images/icon-time.png" alt=""></div>
							<div class="col-md-3">
								<h4 class="pull-left"> COURSE LESSON FOR YOUR CHANGE<span>   03/03/2018 - 09:00 AM</span></h4>
							</div>
							<div class="col-md-4">
								<div class="pull-left" id="clock"></div>
							</div>
							<div class="col-md-2"><a class="pull-right" href="">   Details</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php include('footer.php'); ?>