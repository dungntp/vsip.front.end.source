
    <footer>
        <div class="container">
            <div class="pull-left"> © 2018 Copyright by VSIP</div><a class="pull-right" href="" title="">Contact us</a>
        </div>
    </footer>
    <a class="cd-top" href="#0">Top</a>
    <script type="text/javascript" src="js/app.bundle.js"></script>
    <script type="text/javascript" src="js/libs/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="js/libs/jquery.themepunch.revolution.min.js"></script>
    <script>
        var revapi;

        jQuery(document).ready(function() {

            revapi = jQuery('.tp-banner').revolution(
            {
                delay:6000,
                startwidth:1170,
                startheight:700,
                hideThumbs:10,
                fullWidth:"on",
                forceFullWidth:"on",
            });

            revapi = jQuery('.tp-banner2').revolution(
            {
                delay:8000,
                startwidth:1170,
                startheight:700,
                hideThumbs:10,
                fullWidth:"on",
                forceFullWidth:"on",
            });


            jQuery('a[href^="#"]').on('click', function (e) {
                e.preventDefault();

                var target = this.hash;
                var $target = jQuery(target);

                jQuery('html, body').animate({
                    'scrollTop': $target.offset().top
                }, 1000, 'swing');
            });

        });
    </script>
</body>
</html>